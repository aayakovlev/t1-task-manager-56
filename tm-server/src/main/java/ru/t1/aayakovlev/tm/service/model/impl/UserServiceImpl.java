package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.UserEmailExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserLoginExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.exception.field.RoleEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.repository.model.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.model.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.model.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.model.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.model.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
public final class UserServiceImpl extends AbstractBaseService<User, UserRepository> implements UserService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Override
    protected UserRepository getRepository() {
        return context.getBean(UserRepositoryImpl.class);
    }

    @NotNull
    private ProjectRepository getProjectRepository() {
        return context.getBean(ProjectRepositoryImpl.class);
    }

    @NotNull
    private TaskRepository getTaskRepository() {
        return context.getBean(TaskRepositoryImpl.class);
    }

    @NotNull
    private SessionRepository getSessionRepository() {
        return context.getBean(SessionRepositoryImpl.class);
    }


    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @Nullable User resultModel;
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (isEmailExists(login)) throw new UserEmailExistsException();
        @Nullable User resultModel;
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @Nullable User resultModel;
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), role);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser;
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultUser;
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            return modelRepository.isLoginExists(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
             return modelRepository.isEmailExists(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void remove(@Nullable final User model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final UserRepository modelRepository = getRepository();
        @NotNull final TaskRepository taskRepository = getTaskRepository();
        @NotNull final ProjectRepository projectRepository = getProjectRepository();
        @NotNull final SessionRepository sessionRepository = getSessionRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(model.getId());
            projectRepository.clear(model.getId());
            sessionRepository.clear(model.getId());
            modelRepository.removeById(model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    public @NotNull User update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
