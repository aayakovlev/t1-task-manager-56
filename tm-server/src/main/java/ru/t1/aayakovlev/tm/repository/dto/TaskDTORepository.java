package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;

public interface TaskDTORepository extends ExtendedDTORepository<TaskDTO> {

    @NotNull
    TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );


}
