package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.repository.model.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.service.model.SessionService;

@Service
public final class SessionServiceImpl extends AbstractExtendedService<Session, SessionRepository>
        implements SessionService {

    @NotNull
    @Override
    protected SessionRepository getRepository() {
        return context.getBean(SessionRepositoryImpl.class);
    }

}
