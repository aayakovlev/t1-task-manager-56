package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.field.DescriptionEmptyException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.NameEmptyException;
import ru.t1.aayakovlev.tm.exception.field.StatusEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectDTOServiceImpl extends AbstractExtendedDTOService<ProjectDTO, ProjectDTORepository> implements ProjectDTOService {

    @NotNull
    @Override
    protected ProjectDTORepository getRepository() {
        return context.getBean(ProjectDTORepository.class);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable ProjectDTO resultEntity;
        @NotNull final ProjectDTORepository entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.create(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable ProjectDTO resultEntity;
        @NotNull final ProjectDTORepository entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.create(userId, name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable ProjectDTO resultProject = findById(userId, id);
        resultProject.setStatus(status);
        resultProject = update(resultProject);
        return resultProject;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public ProjectDTO update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable ProjectDTO model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
