package ru.t1.aayakovlev.tm.exception.auth;

public final class PasswordIncorrectException extends AbstractAuthException {

    public PasswordIncorrectException() {
        super("Error! Entered password incorrect for earlier entered user...");
    }

}
