package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.SystemEndpoint;
import ru.t1.aayakovlev.tm.service.CommandService;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected CommandService commandService;

    @NotNull
    @Autowired
    protected SystemEndpoint systemEndpoint;

}
