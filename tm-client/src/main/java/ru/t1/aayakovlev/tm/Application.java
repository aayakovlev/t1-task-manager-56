package ru.t1.aayakovlev.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.aayakovlev.tm.component.ClientBootstrap;
import ru.t1.aayakovlev.tm.config.ClientConfig;

public final class Application {

    @SneakyThrows
    public static void main(@NotNull String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfig.class);
        @NotNull final ClientBootstrap bootstrap = context.getBean(ClientBootstrap.class);
        bootstrap.run(args);
    }

}
