package ru.t1.aayakovlev.tm.service.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.service.CommandService;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class CommandServiceImpl implements CommandService {

    @NotNull
    @Autowired
    private CommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getArgumentCommands() {
        return commandRepository.getArgumentCommands();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
