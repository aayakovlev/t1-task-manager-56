package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataBackupSaveRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-save";

    @NotNull
    private static final String DESCRIPTION = "Save backup data.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        domainEndpoint.backupSave(new DataBackupSaveRequest(getToken()));
    }

}
