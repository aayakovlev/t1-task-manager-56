package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Logout user session.";

    @NotNull
    public static final String NAME = "logout";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());

        authEndpoint.logout(request);
        setToken(null);
    }

}
