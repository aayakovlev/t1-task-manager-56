package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        domainEndpoint.yamlLoad(new DataYamlLoadFasterXmlRequest(getToken()));
    }

}
